package com.sap.clients.green;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import org.json.simple.JSONObject;

import com.sap.clients.util.PropertiesReader;

/**
 * G1 client (green)
 * @author Nihar
 *
 */
public class G1 {

	public static void main(String[] args) {

		final int clientPort = Integer.parseInt(PropertiesReader.getProperty("client.g1.port"));
		final int serverPort = Integer.parseInt(PropertiesReader.getProperty("gateway.port"));
		final String host = PropertiesReader.getProperty("gateway.host");
		JSONObject jsonData = new JSONObject();
		jsonData.put("from", "G1");
		jsonData.put("type", "green");
		jsonData.put("to", "B1");
		jsonData.put("message", "Your Transaction is completed ");

		//Listener thread
		Thread t = new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						ServerSocket ss = new ServerSocket(clientPort);
						System.out.println("[G1] Listening at port: " + clientPort);
						Socket s = ss.accept();// establishes connection
						ObjectInputStream objectInputStream = new ObjectInputStream(s.getInputStream());

						Object data = objectInputStream.readObject();
						System.out.println("[G1] message received= " + data.toString());
						objectInputStream.close();
						s.close();
						ss.close();
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e);
						System.exit(0);
					}
				}
			}
		});
		t.start();
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		try {
			sendMessage(jsonData, host, serverPort);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//sending multiple request
	private static void sendMessage(JSONObject jsonData, String host, int serverPort)
			throws UnknownHostException, IOException {
		Socket socket = new Socket(host, serverPort);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
		for (int i = 0; i < 500; i++) {
			try {

				objectOutputStream.writeObject(jsonData);
				Thread.sleep(50);
			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}

		}
		objectOutputStream.close();
		socket.close();
	}
}
