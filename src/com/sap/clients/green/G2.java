package com.sap.clients.green;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import com.sap.clients.util.PropertiesReader;
/**
 * G2 Client (green)
 * @author Nihar
 *
 */
public class G2 {

	public static void main(String[] args) {
		final int clientPort = Integer.parseInt(PropertiesReader.getProperty("client.g2.port"));
		final String host = PropertiesReader.getProperty("gateway.host");
		final int serverPort = Integer.parseInt(PropertiesReader.getProperty("gateway.port"));
		
		//Listener thread
		Thread t = new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						ServerSocket ss = new ServerSocket(clientPort);
						System.out.println("[G2] Listening at port: " + clientPort);
						Socket s = ss.accept();// establishes connection
						ObjectInputStream objectInputStream = new ObjectInputStream(s.getInputStream());

						Object data = objectInputStream.readObject();
						System.out.println("[G2] message received= " + data.toString());
						objectInputStream.close();
						s.close();
						ss.close();
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e);
						System.exit(0);
					}
				}
			}
		});
		t.start();
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		try {
			sendMessage(host, serverPort);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void sendMessage(String host, int serverPort) throws UnknownHostException, IOException {
		String xmlString = "<messages><from>G2</from><type>green</type><to>B2</to>";
		xmlString += "<message>The records are updated..</message></messages>";
		Socket socket = new Socket(host, serverPort);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
		
		//sending multiple request
		for (int i = 0; i < 500; i++) {
			try {
				objectOutputStream.writeObject(xmlString);
				Thread.sleep(50);
			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
		}
		objectOutputStream.close();
		socket.close();
	}
}
