package com.sap.clients.blue;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import com.sap.clients.util.PropertiesReader;

/**
 * B1 client
 * @author Nihar
 *
 */
public class B1 {

	public static void main(String[] args) {
		final int clientPort = Integer.parseInt(PropertiesReader.getProperty("client.b1.port"));
		final int serverPort = Integer.parseInt(PropertiesReader.getProperty("gateway.port"));
		final String host = PropertiesReader.getProperty("gateway.host");
		
		String xmlString = "<messages><from>B1</from><type>blue</type><to>G2</to>";
		xmlString += "<message>The records are updated..</message></messages>";
		
		Thread t = new Thread(new Runnable() {

			//Listener thread
			public void run() {
				while (true) {
					try {
						ServerSocket ss = new ServerSocket(clientPort);
						System.out.println("[B1] Listening at port: " + clientPort);
						Socket s = ss.accept();// establishes connection
						ObjectInputStream objectInputStream = new ObjectInputStream(s.getInputStream());

						Object data = objectInputStream.readObject();
						System.out.println("[b1] message received= " + data.toString());
						objectInputStream.close();
						s.close();
						ss.close();
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e);
						System.exit(0);
					}
				}
			}
		});
		t.start();
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
			sendMessage(xmlString, host, serverPort);
	}

	static void sendMessage(String xmlString, String host, int serverPort) {
		
		
		// sending multiple request in loop
		for (int i = 0; i < 500; i++) {
			try {
				Socket socket = new Socket(host, serverPort);
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
				objectOutputStream.writeObject(xmlString);
				objectOutputStream.close();
				socket.close();
				Thread.sleep(50);
			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
		}
		
	}
}
