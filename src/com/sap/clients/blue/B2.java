package com.sap.clients.blue;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import org.json.simple.JSONObject;
import com.sap.clients.util.PropertiesReader;

/**
 * B2 client (blue)
 * @author Nihar
 *
 */
public class B2 {

	public static void main(String[] args) {
		final int clientPort = Integer.parseInt(PropertiesReader.getProperty("client.b2.port"));
		final int serverPort = Integer.parseInt(PropertiesReader.getProperty("gateway.port"));
		final String host = PropertiesReader.getProperty("gateway.host");
		JSONObject jsonData = new JSONObject();
		jsonData.put("from", "B2");
		jsonData.put("type", "blue");
		jsonData.put("to", "G1");
		jsonData.put("message", "Your Transaction is completed ");

		Thread t = new Thread(new Runnable() {

			// Listener thread
			public void run() {
				while (true) {
					try {
						ServerSocket ss = new ServerSocket(clientPort);
						System.out.println("[B2] Listening at port: " + clientPort);
						Socket s = ss.accept();// establishes connection
						ObjectInputStream objectInputStream = new ObjectInputStream(s.getInputStream());

						Object data = objectInputStream.readObject();
						System.out.println("[b2] message received= " + data.toString());
						objectInputStream.close();
						s.close();
						ss.close();
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e);
						System.exit(0);
					}
				}
			}
		});
		t.start();
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		try {
			sendMessage(jsonData, host, serverPort);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	static void sendMessage(JSONObject jsonData, String host, int serverPort) throws UnknownHostException, IOException {

		//sending multiple requests
		for (int i = 0; i < 500; i++) {
			try {
				Socket socket = new Socket(host, serverPort);
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
				objectOutputStream.writeObject(jsonData);
				Thread.sleep(50);
			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
		}
	}
}
